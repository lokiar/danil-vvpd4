import math
from sys import exit


def bord_2():
    print('-' * 80)


def bord_1():
    print('=' * 80)


def checkout_input():
    while True:

        print('ПРИМЕР ВВОДА: #54F04E 50')
        person_input = input('Ведите через пробел цвет и процент осветления или затемнения\n'
                             '===> ').split()
        if person_input[1].isdigit():
            nambers_hex = '0123456789abcdefABCDEF'
            flag_old = 6
            flag_new = 0
            for i in person_input[0][1:]:
                if i in nambers_hex:
                    flag_new += 1

            if flag_new == flag_old and len(person_input[0]) == 7 and person_input[0][0] == "#" and \
                    0 <= int(person_input[1]) <= 100:
                return tuple(person_input)

        print('Ошибка')
        end_input = input('Если хотите выйти введи yes, иначе enter: ')
        if end_input == 'yes':
            exit()
            bord_2()


def lighten(color, percent):
    if color[1:] == '000000':
        print('Не могу получить такой цвет :(')
    if int(percent) == 100:
        print('Полученый цвет: #000000')
    if int(percent) == 0:
        print(f'Полученый цвет: {color}')
    if color[1:] != '000000' and 1 <= int(percent) <= 99:
        total_color = '#'
        color_new = color[1:]
        for i in 0, 2, 4:
            value = color_new[i] + color_new[i + 1]
            value_hex = int(value, 16)

            namber = math.floor(value_hex - (value_hex * (int(percent) / 100)))
            total_color += hex(namber)[2:]
        print(f'Полученый цвет: {total_color}')


def darken(color, percent):
    if color[1:].upper() == 'FFFFFF':
        print('Не могу получить такой цвет :(')
    if int(percent) == 100:
        print('Полученый цвет: #FFFFFF')
    if int(percent) == 0:
        print(f'Полученый цвет: {color}')
    if color[1:].upper() != 'FFFFFF' and 1 <= int(percent) <= 99:
        total_color = '#'
        color_new = color[1:]
        for i in 0, 2, 4:
            value = color_new[i] + color_new[i + 1]
            value_hex = int(value, 16)

            namber = math.ceil(value_hex + (value_hex * (int(percent) / 100)))
            total_color += hex(namber)[2:]
        print(f'Полученый цвет: {total_color}')


def totality(color1, percent1):
    print('Затемнения:')
    darken(color1, percent1)
    bord_2()
    print('Осветления:')
    lighten(color1, percent1)


def menu():
    while True:
        bord_1()
        print('МЕНЮ')
        print('1 Затемнения цвета')
        print('2 Осветления цвета')
        print('3 Вывести все выше перечисленое')
        print('4 Выход')
        bord_1()
        input_menu = input('Введите значение из меню\n'
                           '===> ')
        if input_menu == '1':
            a, b = checkout_input()
            bord_2()
            print(f'Исходный цвет: {a}')
            darken(a, b)
        if input_menu == '2':
            a, b = checkout_input()
            bord_2()
            print(f'Исходный цвет: {a}')
            lighten(a, b)
        if input_menu == '3':
            a, b = checkout_input()
            bord_2()
            print(f'Исходный цвет: {a}')
            bord_2()
            totality(a, b)
        if input_menu == '4':
            exit()


def main():
    while True:
        menu()


if __name__ == '__main__':
    main()
print('Danil, ti musor, privet<3')